package cat.itb.catexplorer.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cat.itb.catexplorer.R
import cat.itb.catexplorer.databinding.ItemFavoriteBinding
import cat.itb.catexplorer.view.FavoritesFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class CatAdapter (var cats : MutableList<Cat>, private val listener : FavoritesFragment) : RecyclerView.Adapter<CatAdapter.ViewHolder>() {

    private lateinit var context : Context

    fun add(cat : Cat) {
        cats.add(cat)
        notifyItemInserted(this.cats.size-1)
    }

    fun remove(cat : Cat) {
        cats.remove(cat)
        notifyItemRemoved(this.cats.size-1)
    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val bind = ItemFavoriteBinding.bind(view)
        fun setListener(cat : Cat) {
            bind.root.setOnClickListener {
                listener.onClick(cat)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_favorite, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cat = cats[position]
        with (holder) {
            setListener(cat)
            Glide.with(context)
                .load("https://cataas.com"+cat.url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(bind.image)
        }
    }

    override fun getItemCount(): Int {
        return cats.size
    }

}
