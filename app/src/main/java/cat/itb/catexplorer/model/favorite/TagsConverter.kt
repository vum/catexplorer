package cat.itb.catexplorer.model.favorite

import androidx.room.TypeConverter

class TagsConverter {

    @TypeConverter
    fun fromString(string: String): List<String> {
        return string.split(",")
    }

    @TypeConverter
    fun toString(list: List<String>): String {
        return list.joinToString()
    }

}