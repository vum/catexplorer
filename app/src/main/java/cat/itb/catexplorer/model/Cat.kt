package cat.itb.catexplorer.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Cat (
    @PrimaryKey var id : String,
    var created_at : String,
    var tags : List<String>,
    var url : String
)
