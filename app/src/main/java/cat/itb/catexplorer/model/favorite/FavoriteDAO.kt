package cat.itb.catexplorer.model.favorite

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import cat.itb.catexplorer.model.Cat

@Dao
interface FavoriteDAO {

    @Query("SELECT * FROM Cat")
    fun getKittens(): MutableList<Cat>

    @Insert
    fun add(cat: Cat)

    @Delete
    fun del(cat: Cat)

}
