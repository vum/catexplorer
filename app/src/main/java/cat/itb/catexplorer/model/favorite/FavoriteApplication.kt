package cat.itb.catexplorer.model.favorite

import android.app.Application
import androidx.room.Room

class FavoriteApplication: Application() {
    companion object {
        lateinit var database: FavoriteDB
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(
            this,
            FavoriteDB::class.java,
            "FavoriteDB"
        ).build()
    }
}