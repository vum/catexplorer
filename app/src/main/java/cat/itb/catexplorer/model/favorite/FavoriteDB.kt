package cat.itb.catexplorer.model.favorite

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import cat.itb.catexplorer.model.Cat

@Database(entities = arrayOf(Cat::class), version = 2)
@TypeConverters(TagsConverter::class)
abstract class FavoriteDB: RoomDatabase() {
    abstract fun favoriteDAO(): FavoriteDAO
}