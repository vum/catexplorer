package cat.itb.catexplorer.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cat.itb.catexplorer.model.API
import cat.itb.catexplorer.model.Cat
import cat.itb.catexplorer.model.favorite.FavoriteApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CatViewModel : ViewModel() {

    var generated = MutableLiveData<String>()
    var cat = MutableLiveData<Cat>()

    private val api = API.create()
    var tags = MutableLiveData<List<String>>()

    var cats = MutableLiveData<MutableList<Cat>>()

    init {
        generated.value = "/cat?json=true"
        /*cats.value = mutableListOf(
            Cat("5fa09fe3d756950018646202", "2020-11-03T00:10:11.205Z", listOf(), "/cat/5fa09fe3d756950018646202"),
            Cat("6104b503e10e00001202111a", "2021-07-31T02:27:14.991Z", listOf(), "/cat/6104b503e10e00001202111a"),
            Cat("6010b5cc47d128001b7bbb81", "2021-01-27T00:37:32.643Z", listOf("small", "meme"), "/cat/6010b5cc47d128001b7bbb81/says/aaaaaa")
        )*/

        cats.value = mutableListOf()

        viewModelScope.launch {
            //cats.value =
            withContext (Dispatchers.IO) {
                val tmpCats = FavoriteApplication.database.favoriteDAO().getKittens()
                // i wish androidx.room developers a very die
                for (cat in tmpCats) cats.value!!.add(cat)
            }
            val response = api.getTags()
            tags.value = if (response.isSuccessful) response.body() else listOf()
        }
    }

    fun load(endpoint : String) {
        viewModelScope.launch {
            val response = api.get(endpoint)
            cat.value = if (response.isSuccessful) response.body() else null
        }
    }

    fun addFavorite() {
        viewModelScope.launch {
            withContext (Dispatchers.IO) {
                FavoriteApplication.database.favoriteDAO().add(cat.value!!)
            }
        }
        cats.value!!.add(cat.value!!)
        cats.value = cats.value
    }

    fun removeFavorite() {
        viewModelScope.launch {
            withContext (Dispatchers.IO) {
                FavoriteApplication.database.favoriteDAO().del(cat.value!!)
            }
        }
        cats.value!!.remove(cat.value)
        cats.value = cats.value
    }

    fun isFav() : Boolean {
        for (kitten in cats.value!!)
            if (kitten.id == cat.value!!.id)
                return true
        return false
    }

}