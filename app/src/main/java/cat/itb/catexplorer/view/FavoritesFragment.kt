package cat.itb.catexplorer.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.catexplorer.R
import cat.itb.catexplorer.databinding.FragmentFavoritesBinding
import cat.itb.catexplorer.model.Cat
import cat.itb.catexplorer.model.CatAdapter
import cat.itb.catexplorer.viewmodel.CatViewModel

class FavoritesFragment : Fragment() {

    private lateinit var bind : FragmentFavoritesBinding
    private val model : CatViewModel by activityViewModels()
    private lateinit var catAdapter : CatAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentFavoritesBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.window.statusBarColor = ContextCompat.getColor(context!!, R.color.green_200)
        activity!!.window.navigationBarColor = ContextCompat.getColor(context!!, R.color.green_200)

        catAdapter = CatAdapter(model.cats.value!!, this)

        bind.favorites.apply {
            setHasFixedSize(false)
            adapter = catAdapter
        }

        bind.generate.setOnClickListener {
            findNavController().navigate(R.id.action_favorites_to_generate)
        }
    }

    fun onClick(cat : Cat) {
        println(cat)
        model.cat.value = cat
        findNavController().navigate(R.id.action_favorites_to_detail)
    }

}