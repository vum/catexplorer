package cat.itb.catexplorer.view

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import cat.itb.catexplorer.R
import cat.itb.catexplorer.databinding.FragmentDetailBinding
import cat.itb.catexplorer.model.favorite.FavoriteApplication
import cat.itb.catexplorer.viewmodel.CatViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class DetailFragment : Fragment() {

    private lateinit var bind : FragmentDetailBinding
    private val model : CatViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = FragmentDetailBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.window.statusBarColor = ContextCompat.getColor(context!!, R.color.blue_200)
        activity!!.window.navigationBarColor = ContextCompat.getColor(context!!, R.color.blue_200)

        bind.favorite!!.setOnCheckedChangeListener { _, isChecked ->
            if (!model.isFav() and isChecked) model.addFavorite()
            else if (!isChecked) model.removeFavorite()
        }

        model.cat.observe(this, {
            bind.favorite!!.isChecked = model.isFav()
            if (model.cat.value != null) {
                Glide.with(context!!)
                    .load("https://cataas.com" + model.cat.value!!.url)
                    .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(bind.cat)
                with (bind) {
                    catId.setText(model.cat.value!!.id)
                    catCreatedAt.setText(model.cat.value!!.created_at)
                    catTags.setText(model.cat.value!!.tags.joinToString())
                }
            } else {
                Toast.makeText(context, "There was an error getting the cat!", Toast.LENGTH_SHORT).show()
                with (bind) {
                    cat.setImageResource(android.R.drawable.progress_indeterminate_horizontal)
                    catId.setText("")
                    catCreatedAt.setText("")
                    catTags.setText("")
                }
            }
        })

        bind.refresh!!.setOnRefreshListener {
            model.load(model.generated.value.toString())
            findNavController().popBackStack(R.id.detailFragment, true)
            findNavController().navigate(
                R.id.detailFragment,
                null,
                navOptions {
                    anim {
                        exit = R.anim.slide_out_left
                        popEnter = android.R.anim.slide_in_left
                        popExit  = android.R.anim.slide_out_right
                    }
                }
            )
        }



        bind.download.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                val image = Glide.with(context!!)
                    .asBitmap()
                    .load("https://cataas.com" + model.cat.value!!.url)
                    .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                    .error(android.R.drawable.stat_notify_error)
                    .submit()
                    .get()

                val imageFileName = model.cat.value!!.id + ".jpg"
                val storageDir = File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString(),
                    "CatExplorer"
                )

                if (storageDir.exists() || storageDir.mkdirs()) {
                    val imageFile = File(storageDir, imageFileName)
                    try {
                        val fOut: OutputStream = FileOutputStream(imageFile)
                        image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                        fOut.close()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    activity!!.runOnUiThread {
                        Toast.makeText(
                            context,
                            "Image downloaded! Path: " + imageFile.getAbsolutePath(),
                            Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

    }

}