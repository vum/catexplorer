package cat.itb.catexplorer.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.catexplorer.R
import cat.itb.catexplorer.databinding.FragmentGeneratorBinding
import cat.itb.catexplorer.model.API
import cat.itb.catexplorer.viewmodel.CatViewModel

class GeneratorFragment : Fragment() {

    private lateinit var bind : FragmentGeneratorBinding
    private val model : CatViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = FragmentGeneratorBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.window.statusBarColor = ContextCompat.getColor(context!!, R.color.red_200)
        activity!!.window.navigationBarColor = ContextCompat.getColor(context!!, R.color.red_200)

        model.tags.observe(this, {
            bind.tag.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, model.tags.value!!)
        })

        bind.submit.setOnClickListener {
            var tag = bind.tag.selectedItem.toString()
            if (tag != "") tag = "/"+tag
            val gif = if (bind.gif.isChecked) "/gif" else ""
            if (gif != "") tag = "" // oooh :'(
            val says = if (bind.says.text.isNullOrEmpty()) "" else "/says/"+bind.says.text
            val filter = if (bind.filter.text.isNullOrEmpty()) "" else "&fi="+bind.filter.text

            model.generated.value = "/cat" + tag + gif + says + "?json=true" + filter
            model.load(model.generated.value.toString())

            findNavController().navigate(R.id.action_generator_to_detail)
        }

    }

}