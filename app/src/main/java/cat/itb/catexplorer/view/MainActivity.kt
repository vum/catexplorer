package cat.itb.catexplorer.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cat.itb.catexplorer.R

// https://cataas.com/

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}